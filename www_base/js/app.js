// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter',
['ionic', 'starter.controllers', 'starter.services', 'templates'])

.run(function($ionicPlatform, $state, $rootScope, $location, $ionicHistory) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    $ionicPlatform.registerBackButtonAction(function (event) {
      if($ionicHistory.currentStateName() === 'products-home'){
        $state.go('products.tab')
      } else if(
        $ionicHistory.currentStateName() === 'distribchange'){

        console.log('preventDefault')
        event.preventDefault();
      } else {
        $ionicHistory.goBack();
      }
    }, 100);

  });
})

.config(function(
  $stateProvider,
  $urlRouterProvider,
  $logProvider,
  $compileProvider,
  $ionicConfigProvider,
  $animateProvider) {

  //disable debug
  $logProvider.debugEnabled(false);
  $compileProvider.debugInfoEnabled(false);

  //disable jsScrolling
  $ionicConfigProvider.scrolling.jsScrolling(false);

  //set max cache
  $ionicConfigProvider.views.maxCache(1);

  //enable animations explicitly for performance boost
  $animateProvider.classNameFilter( /\banimated\b/ );

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('initial', {
    cache: false,
    url: '/initial',
    templateUrl: 'initial/initial.html',
    controller: 'InitialCtrl'
    }
  )

  .state('login', {
      cache: false,
      url: '/login',
      templateUrl: 'initial/login.html',
      controller: 'LoginCtrl'
    }
  )

  .state('products', {
      url: '/products',
      templateUrl: 'products/products.html',
      controller: 'ProdutosCtrl',
      abstract: true,
    }
  )

  .state('products.tab', {
      url: "/home",
      views: {
        'products-tab': {
          templateUrl: "products/product-tab.html",
          controller: 'ProdutosCtrl'
        }
      }
    })

  .state('products-home', {
      cache: false,
      url: '/product-home',
      templateUrl: 'product-home/product-home.html',
      controller: 'ProductHomeCtrl',
    }
  )

  .state('transfer-balance', {
    cache: false,
    url: '/transfer-balance',
    templateUrl: 'transfer-balance/transfer-balance.html',
    controller: 'TransferCtrl',
  }
)

  .state('transfer-balance-range', {
      cache: false,
      url: '/transfer-balance-range',
      templateUrl: 'transfer-balance/transfer-balance-range.html',
      controller: 'TransferCtrl',
    }
  )

  .state('transfer-balance-success', {
      cache: false,
      url: '/transfer-balance-success',
      templateUrl: 'transfer-balance/transfer-balance-success.html',
      controller: 'TransferCtrl',
    }
  )

  .state('sva', {
    cache: false,
    url: '/sva',
    templateUrl: 'sva/sva.html',
    controller: 'SvaCtrl',
  })

  .state('buy-package', {
      cache: false,
      url: '/buy-package',
      templateUrl: 'buy-package/buy-package.html',
      controller: 'BuyPackageCtrl'
    }
  )

  .state('buy-package-selected', {
      cache: false,
      url: '/buy-package-selected',
      templateUrl: 'buy-package/buy-package-selected.html',
      controller: 'BuyPackageCtrl',
      params: {
        phone: "" ,//default
        id: -1 //default
      }
    }
  )

  .state('buy-package-selected-confirm', {
      cache: false,
      url: '/buy-package-selected-confirm',
      templateUrl: 'buy-package/buy-package-selected-confirm.html',
      controller: 'BuyPackageCtrl',
      params: {
        phoneNumber: '',
        pack: null
      }
    }
  )

  .state('buy-package-success', {
      cache: false,
      url: '/buy-package-success',
      templateUrl: 'buy-package/buy-package-success.html',
      controller: 'BuyPackageCtrl',
      params: {
        phoneNumber: '',
        pack: null
      }
    }
  )

  .state('distribution', {
      url: '/distribution',
      templateUrl: 'distribution/distribution.html',
      controller: 'DistributionCtrl'
    }
  )

  .state('distribchange', {
    url: '/distribution_change',
    templateUrl: 'distribution/distribution-change.html',
    controller: 'DistributionCtrl'
  })
  .state('distribinfo', {
    url: '/distribinfo',
    templateUrl: 'distribution/distribution-info.html',
    controller: 'DistributionCtrl'
  })

  .state('distribsucess', {
    url: '/distribsucess',
    templateUrl: 'distribution/distribution-success.html',
    controller: 'DistributionCtrl'
  })
  .state('historic', {
    url: '/historic',
    templateUrl: 'historic/historic.html',
    controller: 'HistoricCtrl'
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/initial');
  //$urlRouterProvider.otherwise('products-home');
  //$urlRouterProvider.otherwise('/distribution');
  // $urlRouterProvider.otherwise('/transfer-balance')
  //$urlRouterProvider.otherwise('/historic')
  //$urlRouterProvider.otherwise('/buy-package')
});
