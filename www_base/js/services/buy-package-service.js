angular.module('starter.controllers')
.service('items', function() {
    var items = [];

    var selectedItem = null;

    this.add = function(item,index) {
        items.push(item);
        var total = items.length;
        if (total > 1) {
          items.splice(index, 1);
        }

        console.log('add', item)
        if(item) this.selectedItem = item;
    };
    this.lista = function() {
        return items;
    };

    this.getItem = function(){
      console.log('selectedItem', this.selectedItem);
      return this.selectedItem;
    }

})


.service('pkgs', function() {
    var pkgs = [];
    var pkgsService = {};
    this.addPkg = function(pack,index) {
        pkgs.push(pack);
        var total = pkgs.length;
        if (total > 1) {
          pkgs.splice(index, 1);
        }

        this.selectedPack = pack;
        return pack;
    };

    this.getPack = function(){
      return this.pack;
    }

    this.list = function() {
        return pkgs;
    };
});
