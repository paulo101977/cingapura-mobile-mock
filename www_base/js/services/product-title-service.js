angular.module('starter.controllers')
.service('ProductTitleService', function() {
    this.title = "";

    this.setTitle = function(title){
      this.title = title;
    }

    this.getTitle = function(){
      return this.title;
    }
})
