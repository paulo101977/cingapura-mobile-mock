angular.module('starter.controllers')
.service('transferBalanceService', function() {
    this.first = null;
    this.before = -1;
    this.stack = []
    this.sum = 0;
    this.selectedIndex = []
    this.diff = 0;
    this.phoneNumbers = ['','','',''];
    this.colors = []

    this.resetAll = function(){
      this.first = null;
      this.before = -1;
      this.stack = []
      this.sum = 0;
      this.selectedIndex = []
      this.diff = 0;
      this.phoneNumbers = ['','','',''];
    }

    this.setColors = function(colors, data){
      //this.colors = colors;
      var colorsArr = [];

      data.forEach(function(item, index){
        console.log('color')
        if(item){
          colorsArr.push(colors[index])
        }
      })

      this.colors = colorsArr;
    }

    this.getColors = function(){
      return this.colors;
    }

    this.setPhoneNumbers = function(scope){
      var firstUser = scope.firstUser;
      var secondUser = scope.secondUser;
      var fCurrent = firstUser.current;
      var fOriginal = firstUser.value;
      var sCurrent = secondUser.current;
      var sOriginal = secondUser.valeu;

      if(fCurrent > fOriginal){
        this.phoneNumbers[0] = secondUser.phone
        this.phoneNumbers[1] = firstUser.phone
        this.phoneNumbers[2] = secondUser.current
        this.phoneNumbers[3] = firstUser.current
      } else {
        this.phoneNumbers[0] = firstUser.phone
        this.phoneNumbers[1] = secondUser.phone
        this.phoneNumbers[2] = firstUser.current
        this.phoneNumbers[3] = secondUser.current
      }
    }

    this.getPhoneNumbers = function(){
      return this.phoneNumbers;
    }

    this.setDiff = function(diff){
      this.diff = diff;
    }

    this.getDiff = function(){
      return (+this.diff).toFixed(2);
    }

    this.getSum = function(){
      return this.sum;
    }

    this.setSelectedIndex = function(index){
      this.selectedIndex.push(index)
    }

    this.getSelectedIndex = function(){
      return this.selectedIndex
    }

    this.setSum = function(transferData, data, scope){
      var sum = 0;
      var _self = this;
      this.selectedIndex = [] //reset

      if(data && data.length > 0){
        data.forEach(function(item , index){
          if(item){
            _self.setSelectedIndex(index)
            sum += parseFloat(transferData[index].value);
          }
        })
      }
      this.sum = sum;
    }

    this.pushStack = function(index){
      this.stack.push(index)
    }

    //remove first element
    this.shiftStack = function(){
      return this.stack.shift()
    }

    this.popStack = function(){
      return this.stack.pop()
    }

    this.getStack = function(){
      return this.stack
    }


    this.setFirst = function(first) {
        this.first = first;//the id
    };

    this.getFirst = function() {
        return this.first;
    };

    this.setBefore = function(index){
      this.before = index;
    }

    this.getBefore = function(){
      return this.before;
    }

    this.searchBeforeAndReturnThis = function(data){

      if(typeof data !== 'undefined' && data.length > 0 ){
          return this.shiftStack();
      }
      //not found
      return -1;
    }

    this.getCountChecked = function(data){
      var count = 0;
      data.forEach(function(item,index){
        if(item) count++;
      })

      return count;
    }
});
