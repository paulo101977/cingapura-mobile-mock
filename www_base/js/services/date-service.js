angular.module('starter.controllers')
.service('dateService', function() {
    this.currentDate = null; //it should be nullable

    function formatNumber(number){
      if(number < 10) number = '0' + number;

      return number;
    }

    this.setCurrentDate = function(date) {
        this.currentDate = date;
    };

    this.getCurrentDate = function() {
        if(this.currentDate !== null){
          return this.currentDate;
        }

        var current = new Date();
        var day = +current.getDate();
        var year = +current.getFullYear();
        var month = +(current.getMonth() + 1); //1 to 12
        var dateStr = "";
        var separator = "/";

        if(month > 12){
          month = 0;
          year += 1;
        } else {
          if(month === 1 && day > 28){//february
            day -= 2;
          }
          month += 1;
        }

        day = formatNumber(day);
        month = formatNumber(month);

        // dd/mm/yy
        //dateStr = 13 + separator + month + separator + year;
        dateStr = "13" + separator + '06' + separator + '2018';
        //dateStr = day + separator + month + separator + year;

        //
        this.currentDate = dateStr;

        return this.currentDate;
    };
});
