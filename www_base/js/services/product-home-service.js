angular.module('starter.controllers')
.service('productModalService', function() {
    this.open = true;

    this.setOpen = function() {
        this.open = false;
    };

    this.getOpen = function() {
        return this.open;
    };
});
