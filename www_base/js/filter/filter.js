angular.module('starter.controllers')
.filter('decimal2comma', [
  function() {
    return function(input) {
        if(input){
          input = parseFloat(input).toFixed(2)
        }

        if(input == parseInt(input))
          return parseInt(input);//without comma
        else //add comma to string
          return (input)?input.toString().trim().replace(".",","):"0";
    };
  }
])

.filter('distance', function () {
   var conversionKey = {
       MB: {
           GB: 0.001,
           MB:1
       },
       GB: {
           GB:1,
           MB: 1000
       }
   };

   return function (distance, from, to) {
       return distance * conversionKey[from][to] + to;

  };
});
