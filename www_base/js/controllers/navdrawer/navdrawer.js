angular.module('starter.controllers')
.controller('DrawerCTRL', [ '$scope' ,'$state', '$ionicSideMenuDelegate' ,
  function($scope, $state, $ionicSideMenuDelegate) {

  $scope.closeNavDrawer = function(){
    $ionicSideMenuDelegate.toggleLeft(false);
  }

  $scope.buyPackage = function(){
    $ionicSideMenuDelegate.toggleLeft(false);
    $state.go('buy-package');
  }

  $scope.exitApp = function(){
    console.log('exit app')

    ionic.Platform.exitApp();
  }
}])
