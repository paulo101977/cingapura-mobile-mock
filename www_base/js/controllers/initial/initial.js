angular.module('starter.controllers')
.controller('InitialCtrl', [ '$scope' ,'$state', function($scope, $state) {

    // function to submit the form after all validation has occurred
    $scope.goLogin = function(){
        $state.go('login');
      }
  }

])
