angular.module('starter.controllers')
.controller('LoginCtrl', [ '$scope' ,'$state', function($scope, $state) {

    // function to submit the form after all validation has occurred
    $scope.submitForm = function(isValid) {

    // check to make sure the form is completely valid
    if (isValid) {
        $state.go('products.tab');
    }

  };

}])
