angular.module('starter.controllers')
.controller('BuyPackageCtrl', [
  '$rootScope' ,'$scope' ,'$state','$ionicHistory','items','pkgs', '$timeout' , '$stateParams',
  function($rootScope, $scope, $state, $ionicHistory,items,pkgs, $timeout, $stateParams) {

    $scope.person = data.values;

    $scope.buyPackages = packages.package;
    $scope.transferData = $rootScope.transferData;

    //enable back button
    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
     viewData.enableBack = true;
    });

    $scope.lista = items.lista();
    $scope.add = function(item,index){

      //that is string
      if(typeof item === 'string') item = JSON.parse(item);
      items.add(item,item.id);
      if(item) {
          $scope.goToSelectPkg(item.phone , item.id);
      }

    };

    $scope.addPackageAndConfirm = function(){
      $scope.goToSelectPkgConfirm($scope.selectedPack);
    }

    $scope.list = pkgs.list();
    //$scope.addPkg = pkgs.addPkg;
    $scope.addPkg = function(pack){
      if(typeof pack == 'string') pack = JSON.parse(pack);
      pkgs.addPkg(pack);
      $scope.selectedPack = pack;
    }

    //receive data on change route
    if(!$scope.selectedItem) $scope.selectedItem = $stateParams.phone;
    $scope.itemId = $stateParams.id;
    $scope.pack = $stateParams.pack;
    $scope.phoneNumber = $stateParams.phoneNumber;

    $scope.changeDotByComma = function(str){
      if(typeof str === 'undefined') return '0,00';
      str = '' + str;

      //integer
      if(parseInt(str) == str){
        str = str + ',00';
      }
      //float
      else {
        var intValue = parseInt(str);
        var floatValue = (parseFloat(str) - intValue).toPrecision(2)*100;
        floatValue = parseInt((floatValue/100)*100);
        str = intValue + ',' + floatValue;
      }

      return str.replace('.',',').trim();
    }

    $scope.formatNumber = function(number){
      if(!number) return '';

      number = (number + '').trim();

      if(number < 1000) {
        return number + ' MB'
      }
      else return number/1000 + ' GB';
    }

   $scope.goToSelectPkg = function (phone , id) {
     $state.go('buy-package-selected',{phone:phone , id: id});
   };

   $scope.goToSelectPkgConfirm = function(pack) {
     console.log('goToSelectPkgConfirm')
     console.log($scope)

     $state.go('buy-package-selected-confirm',
     {
       pack: pack,
       phoneNumber: $scope.selectedItem
     });

   };
   $scope.goToBuySuccess = function() {

     var packValue = +$scope.pack.valuePkg;
     var transferData = $rootScope.transferData;
     var phoneNumber = $scope.phoneNumber;


     transferData.forEach(function(item){
       if(item.phone === phoneNumber){
         var pack = +item.package;
         var value = +item.value;
         pack += packValue;

         item.package = pack;
         item.value = value + (packValue/1000);
         //console.log('added')
       }
     })

     $state.go('buy-package-success',
      {
        phoneNumber: $scope.phoneNumber,
        pack: $scope.pack
      });

   };
   $scope.goToBuy = function () {
     $state.go('buy-package');
   };
   $scope.goToInit = function () {
     $state.go('products-home');
   };

}])
