angular.module('starter.controllers')
.controller(
  'ProdutosCtrl', [ '$rootScope', '$scope' ,'$state', 'dateService', 'ProductTitleService',
  function($rootScope, $scope, $state, dateService, ProductTitleService) {

    $scope.currentDate = dateService.getCurrentDate();

    //$scope.productName = ProductTitleService.getTitle();

    $scope.goToProductHomeAndSetTitle = function (name){
      //$rootScope.productName = name;

      if(name){
        ProductTitleService.setTitle(name)
      }

      $rootScope.productName = ProductTitleService.getTitle();

      $state.go('products-home')
    }
}])
