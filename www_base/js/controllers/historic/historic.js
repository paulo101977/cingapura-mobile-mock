var historicData = [
  {
    data: 'Todas as ações'
  },
  {
    data: 'Pacote extra'
  },{
    data: 'Distribuição'
  },{
    data: 'Transferência'
  }
]


var descrTransfer = `10MB de (21) 99992-8192 para (21) 91882-0122`
var descrBuy = `Compra de pacote de internet`
var descrDistr =
`5GB para (21) 9999-7878<br>
5GB para (21) 9999-7878<br>
5GB para (21) 9999-7878`

var infoDataOrigin = [
  {
    title: 'Pacote Extra de Internet',
    description: descrBuy,
    type: 0
  },
  {
    title: 'Distriruição de internet',
    description: descrDistr,
    type: 1
  },
  {
    title: 'Transferência de dados',
    description: descrTransfer,
    type: 2
  },
]

var infoData = [];

function random(start, end){
  var rFloat = Math.random()*(end - start);
  return Math.floor(rFloat) + start;
}

function randomDate(){
  var rYear = 2017;
  var rMonth = random(1, 13);
  var rDay = rMonth !== 2 ? random(1, 31) : random(1, 29);
  var rHour = random(0, 24);
  var rMinute = random(0, 60);
  return new Date(2018, 3, 16, rHour, rMinute, 0, 0);
}

function randomData(){
  var count = 15;
  infoData = [];

  for(count ; count >= 0; count--){
    var rInt = random(0, Math.random() * infoDataOrigin.length + 1);
    var cloned = Object.assign({}, infoDataOrigin[rInt]);

    infoData.push(cloned)
  }

  //sorted by date
  infoData.sort(sortDate)

  var cloned1 = infoDataOrigin.slice()[0]
  cloned1.date = randomDate();

  var cloned2 = infoDataOrigin.slice()[1]
  cloned2.date = randomDate();

  var cloned3 = infoDataOrigin.slice()[2]
  cloned3.date = randomDate();

  infoData.unshift(cloned3)
  infoData.unshift(cloned2)
  infoData.unshift(cloned1)

  return infoData.slice();
}

function random1(){
  var data  = []

  var cloned1 = infoDataOrigin.slice()[0]
  cloned1.date = randomDate();

  var cloned2 = infoDataOrigin.slice()[0]
  cloned2.date = randomDate();

  var cloned3 = infoDataOrigin.slice()[0]
  cloned3.date = randomDate();

  data.push(cloned1)
  data.push(cloned2)
  data.push(cloned3)

  return data;
}

function random2(){
  var data  = []

  var cloned1 = infoDataOrigin.slice()[1]
  cloned1.date = randomDate();

  var cloned2 = infoDataOrigin.slice()[1]
  cloned2.date = randomDate();

  var cloned3 = infoDataOrigin.slice()[1]
  cloned3.date = randomDate();

  data.push(cloned1)
  data.push(cloned2)
  data.push(cloned3)

  return data;
}

function random3(){
  var data  = []

  var cloned1 = infoDataOrigin.slice()[2]
  cloned1.date = randomDate();

  var cloned2 = infoDataOrigin.slice()[2]
  cloned2.date = randomDate();

  var cloned3 = infoDataOrigin.slice()[2]
  cloned3.date = randomDate();

  data.push(cloned1)
  data.push(cloned2)
  data.push(cloned3)

  return data;
}


function sortDate(a, b){
  return a.date - b.date
}
angular.module('starter.controllers')
    .controller('HistoricCtrl', [ '$scope' ,'$state', function($scope, $state) {

        $scope.selectData = historicData;
        $scope.selected = "Todas as ações";
        $scope.active = false;
        $scope.have = -1;
        $scope.filterdBy = "Filtros:"
        $scope.infoData = randomData();
        $scope.search = '';


        $scope.formatDate = function(dateObj){
          dateObj = +dateObj;
          if(dateObj < 10){
            return '0' + dateObj
          }
          return dateObj;
        }


        $scope.open = function(){
          $scope.active = !$scope.active;
        }

        $scope.setSelected = function(selected, index){
          $scope.selected = selected.data;
          $scope.active = false;
          $scope.have = selected.have;
          $scope.filterdBy = "Filtrar por:"

          --index;

          $scope.search = (index >= 0) ? index : '';
          //console.log('search', $scope.search)

          console.log('index', index)
          if(index == -1){
            $scope.infoData= randomData();
          } else if(index == 0){
            $scope.infoData= random1();
          } else if(index == 1){
            $scope.infoData= random2();
          } else if(index == 2){
            $scope.infoData= random3();
          }
        }

        $scope.getClass = function(index){
          index = +index;

          switch (true) {
            case index === historicData.length - 1:
              return 'last';
              break;
            case index === 0:
              return 'first';
              break;
            default:
              return '';
          }
        }

        //enable back button
        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });

        // function to submit the form after all validation has occurred
        $scope.backSateIni = function (){
            $state.go('products-home')
        }

    }

])
