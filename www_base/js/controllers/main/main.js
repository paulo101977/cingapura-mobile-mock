
var data = {
  max: 50,
  values: [{
    id: 0,
    phone: '(21) 99639-9999',
    value: 16.66,
    consumption: 0,
    media: 21,
    block:false,
    max: 0,
    add: 5,
    cor1: '#fe6b03',
    cor2: '#e33465',
    ngconsumption: 'openModalHomeProductGraph()',
  },{
    id: 1,
    phone: '(21) 99639-7565',
    value: 16.66,
    consumption: 17,
    media: 328,
    block:false,
    max: 17,
    cor1: '#00d213',
    cor2: '#00baf7',
    ngconsumption: 'openModalHomeProductGraph()',
  },{
    id: 2,
    phone: '(21) 99639-7562',
    value: 16.66,
    consumption: 0.41,
    media: 12,
    block:false,
    max: 0.41,
    cor1: '#ffd700',
    cor2: '#f8562c',
    ngconsumption: 'openModalHomeProductGraph()',
  }]
}


var packages = {
  package: [{
    idPkg:1,
    pricePkg: 19.90,
    valuePkg: 500,
    units: 'MB'
  },{
    idPkg:2,
    pricePkg: 24.90,
    valuePkg: 1000,
    units: 'MB'
  },{
    idPkg: 3,
    pricePkg: 34.90,
    valuePkg: 2000,
    units: 'MB'
  }]
}

var transferData = [
  {
    phone: '(21) 99639-9999',
    name: 'José',
    value: '0',
    units: 'GB',
    package: 1000,
    consumption: '18 GB',
    ngconsumption: 'openModalHomeProductGraph()',
  },{
    phone: '(21) 99639-7565',
    name: 'Pedro',
    value: '24.59',
    units: 'GB',
    package: 0,
    consumption: '1 GB',
    ngconsumption: 'openModalHomeProductGraph()',
  },{
    phone: '(21) 99639-7562',
    name: 'Roberta',
    value: '0.41',
    units: 'GB',
    package: 500,
    consumption: '6 GB',
    ngconsumption: 'openModalHomeProductGraph()',
  }]

angular.module('starter.controllers')
.controller('MainCTRL', [ '$rootScope' , '$scope' , '$state',
  function($rootScope, $scope, $state) {
      $scope.data = data;
      $scope.hasDistribution = false;
      $scope.showNavbar = true;
      $scope.dataAfterChange = {};
      $scope.packages = packages;
      $scope.transferData = transferData;

      //reference to transferData globally
      $rootScope.transferData = transferData;
  }
])
