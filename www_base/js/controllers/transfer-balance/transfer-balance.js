

angular.module('starter.controllers')
.controller('TransferCtrl',
    ['$rootScope', '$scope','$state','$ionicHistory', 'transferBalanceService',
      function($rootScope, $scope, $state, $ionicHistory, transferBalanceService) {

  //uncheck any checkbox
  $scope.data = [false,false,false,false,false]
  $scope.goToSucess = true;
  $scope.sum = 0;
  $scope.selectedIndex = [];
  $scope.firstUser = {}
  $scope.secondUser = {}
  $scope.showMessage = false;
  $scope.selectedNumber = "";
  $scope.selectedColors = []
  $scope.customUserValue = 0;

  //bar colors:
  var colors = [
    "#fc9318",
    "#f14c3c",
    "#d82481",
    "#aa28e6",
    "#546efb",
  ]


  //the model
  $scope.Item = $scope.data.slice();
  $scope.before = -1;
  $scope.count = 0;
  $scope.diff = 0;

  $scope.resetAll = function(viewData){
    //reset all if route is transfer-balance
    if(viewData.stateId === 'transfer-balance'){
      //uncheck any checkbox
      $scope.data = [false,false,false,false,false];
      $scope.Item = $scope.data.slice();
      transferBalanceService.resetAll();
    }
  }

  /*$("#input-range-transfer").on("change paste keyup", function() {
     console.log($(this).val())
  });*/

  $(document).on('input', '#input-range-transfer', function() {
    var value = +$(this).val();
    var input = $(this);

    if(value < 0.01){
      input.val(0.01)
    } else if(value >= this.max - 0.01){
      input.val(this.max - 0.01)
    }
  });

  $scope.onInputRangeChange = function(customUserValue){
    var fCurrent = 0;
    var fOriginal = 0;
    var sCurrent = 0;
    var sOriginal = 0;
    var firstUser = $scope.firstUser;
    var secondUser = $scope.secondUser;
    var diff = 0;

    var value = +customUserValue; //to integer


    if(value < 0.01){
      value = 0.01;
      $scope.showMessage = true;
      $scope.selectedNumber = $scope.firstUser.phone;
      //$scope.customUserValue = 0.01;
    }
    else if(value >= $scope.sum - 0.01){
      value = $scope.sum - 0.01;
      $scope.showMessage = true;
      $scope.selectedNumber = $scope.secondUser.phone;

      //$('#input-range-transfer').val($scope.sum - 0.1)
    } else {
      $scope.showMessage = false;
    }

    firstUser.current = +value.toFixed(2);
    secondUser.current = +($scope.sum - value).toFixed(2);

    //change divs width
    changePosition(value, $scope.sum);

    //set phone phoneNumbers
    transferBalanceService.setPhoneNumbers($scope)

    fCurrent = +firstUser.current;
    fOriginal = +firstUser.value;
    sCurrent = +secondUser.current;
    sOriginal = +secondUser.value;

    if(fCurrent < fOriginal || fCurrent > fOriginal){
      diff = Math.abs(fCurrent - fOriginal);
    } else {
      diff = 0;
    }

    console.log('set diff')
    console.log(diff)

    transferBalanceService.setDiff(diff);

    //console.log(diff);
  }

  $scope.resetUsersValues = function(){
    $scope.firstUser.current = (+$scope.firstUser.value).toFixed(2);
    $scope.secondUser.current = (+$scope.secondUser.value).toFixed(2);
    $scope.customUserValue = +$scope.firstUser.value;
    $('.input-range-custom input').val(+$scope.firstUser.value)

    changePosition(+$scope.firstUser.value, $scope.sum)
  }

  $scope.onChange = function(index){
    //convert to integer
    index = +index;
    var count = 0;

    //add to stack
    transferBalanceService.pushStack(index)

    var first = transferBalanceService.getFirst()
    if(first === null){
      transferBalanceService.setFirst(index);
      transferBalanceService.setBefore(index);
    }

    count = transferBalanceService.getCountChecked($scope.Item);

    $scope.goToSucess = true;

    if(count > 1) {$scope.goToSucess = false;}

    if(count > 2){
      //try get the data before change the data
      var before = transferBalanceService
        .searchBeforeAndReturnThis($scope.data,$scope.Item,index);

      //uncheck at view
      $scope.Item[before] = false;
    }

    //the clicked element
    $scope.data[index] = !$scope.data[index];
    $scope.data[index] = $scope.Item[index];
  }

  //enable back button
  $scope.$on('$ionicView.beforeEnter',
    function (event, viewData) {

    viewData.enableBack = true;

    //console.log(viewData)

    //reset if route === transfer-balance
    $scope.resetAll(viewData)

    //get current users

    var selected = transferBalanceService.getSelectedIndex()

    //FIXED: remove the change to deep copy
    var firstUser = $scope.firstUser = Object.assign({}, $scope.transferData[selected[0]]);
    var secondUser = $scope.secondUser = Object.assign({}, $scope.transferData[selected[1]]);
    $scope.sum = transferBalanceService.getSum();

    $scope.selectedColors = transferBalanceService.getColors();


    if($scope.firstUser){
      firstUser.current = (+$scope.firstUser.value).toFixed(2);
      secondUser.current = (+$scope.secondUser.value).toFixed(2);
      $scope.customUserValue = $scope.firstUser.current;

      changePosition(
        +firstUser.value,
        $scope.sum,
        $scope.selectedColors
      )

      if($scope.selectedColors.length > 0){
        // console.log('oval1', $('.oval1'))
        var colors = $scope.selectedColors;
        $('.oval1').css('background-color',colors[0])
        $('.oval2').css('background-color',colors[1])
      }

      $scope.diff = transferBalanceService.getDiff();
      $scope.diffSucess = transferBalanceService.getDiff();


      $scope.phoneNumbers = transferBalanceService.getPhoneNumbers();
    }

    //$scope.diff = transferBalanceService.getDiff();
  });

  $scope.goTransferBalanceRange = function (){
    transferBalanceService
      .setSum($scope.transferData, $scope.data, $scope.sum);

    transferBalanceService.setColors(colors, $scope.data)


    $state.go('transfer-balance-range')
  }

  $scope.goTransferBalanceSuccess = function (){
    var firstUser = $scope.firstUser;
    var secondUser = $scope.secondUser;
    var transferData = $rootScope.transferData;

    /**/

    console.log('goTransferBalanceSuccess')

    //value is original
    if(firstUser && firstUser.value == firstUser.current){
      console.log('goTransferBalanceSuccess if')
      //console.log('goTransferBalanceSuccess')
      transferBalanceService.setDiff(0);
      transferBalanceService.setPhoneNumbers($scope)
    }

    //set after !
    transferData.forEach(function(item, index){
      if(item.name === firstUser.name){
        item.value = firstUser.current;
      }

      if(item.name === secondUser.name){
        item.value = secondUser.current;
      }
    })

    $state.go('transfer-balance-success')
  }

  $scope.goToInit = function(){
    //disable back button
    /*$ionicHistory.nextViewOptions({
      disableBack: true
    });*/


    $state.go('products-home')
  }

}])

//value, $scope.sum;
function changePosition(value, sum, colors){
  var width = $('.input-range-custom').width();
  var left = $('.input-range-custom .left');
  var right = $('.input-range-custom .right');
  var lw = left.width();
  var rw = right.width();
  var rValue = sum - value;


  if(colors && colors.length > 0){
    left.css('background', colors[0])
    right.css('background', colors[1])
    var balance = $('.transfer-first .balance-value')
      .css('color', colors[0])
    balance.children().css('color', colors[0])

    balance = $('.transfer-second .balance-value')
      .css('color', colors[1])
    balance.children().css('color', colors[1])
  }

  left.width(width*value/sum)
  right.width(width*rValue/sum)
}
