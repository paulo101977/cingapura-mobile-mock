angular.module('starter.controllers')
.controller('ProductHomeCtrl', [ '$scope' ,'$state','$ionicModal',
'$rootScope', 'productModalService', 'dateService',
 function($scope, $state, $ionicModal,
   $rootScope, productModalService, dateService) {

  //enable functions on menu
  $rootScope.hasPopover = true;

  $scope.currentDate = dateService.getCurrentDate();

  //console.log($scope.currentDate)

  //enable back button
  $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
    viewData.enableBack = true;
  });

  $scope.backSateIni = function (){
    $state.go('products')
  }
  $scope.goCompraPacote = function (){
    $state.go('buy-package')
  }
  $scope.goToHistoric = function (){
    $state.go('historic')
  }
  $scope.goDistribuir = function (){
    $state.go('distribution')
  }
  $scope.goTransferBalance = function (){
    $state.go('transfer-balance')
  }
  $scope.goSva = function (){
    $state.go('sva')
  }



  $scope.options = {
    initialSlide: 1,
    loop: false,
    slidesPerView: 'auto',
    speed: 500,
    centeredSlides: true,
  }

  $scope.adicionaFrase = function (item, index){
    var obj = {
      txt: '',
      lnk: '',
      lnktxt: '',
      ngroute: ''
    }

    //console.log('adicionaFrase', item, index)

    if(item.value == 0)
      obj = {
        txt : 'Atenção! Saldo esgotado.',
        lnktxt: 'Mais internet',
        ngroute: 'openModalInternetFim()',
      }

    else if(item.value < 0.5)
      obj = {
        txt : 'Alto consumo de internet. ',
        lnktxt: 'Mais internet',
        ngroute: 'openModalInternetQuaseFim()',
      }

    else {
      $('#' + index).css({ 'display':'none' });
      // return null
    }

    return obj
  }


  function test(url){
    console.log("url is "+url);
  }

  //deep copy
  $scope.sliderContent = [];

  //console.log('$rootScope in home product')
  //console.dir($rootScope.transferData)


  //angular.copy($scope.$parent.data.values, $scope.sliderContent)
  angular.copy($rootScope.transferData, $scope.sliderContent)

  //console.log('$scope.sliderContent')

  //console.dir($scope.sliderContent)

  $scope.$on("$ionicSlides.sliderInitialized", function(event, dataSlider){
    // data.slider is the instance of Swiper
    $scope.slider = dataSlider.slider;

  });



  $scope.openModalProductHomeDist = function() {
    openModalProductHomeDist($ionicModal, $scope)
  };

  $scope.openModalProductHome = function() {
    openModalProductHome($ionicModal, $scope)
  };

  $scope.openModalHomeProductGraph = function(index, sliderContent) {
    var selected = sliderContent[index]

    openModalHomeProductGraph($ionicModal, $scope, selected)
  };


  $scope.openModalInternetFim = function() {
    openModalInternetFim($ionicModal, $scope, $state)
  };

  $scope.openModalInternetQuaseFim = function() {
    openModalInternetQuaseFim($ionicModal, $scope, $state)
  };

  $scope.openModalInternetStart = function() {
    openModalInternetStart($ionicModal, $scope, $state)
  };


  $ionicModal.fromTemplateUrl(
    'js/directives/product-home/modal-home-product-start.html', {
    scope: $scope,
    animation: 'scale-in'
  }).then(function(modal) {
      var automaticOpen = false;
      //productModalService.setOpen(true)

      //console.log('modal start')
      $rootScope.transferData.forEach(function(item){
        if(item.value == 0){
          automaticOpen = true;
        }
      })

      // test if have open or not the modal
      if(productModalService.getOpen()){
        //modal.show()
      };

      //set to not open
      productModalService.setOpen(false)

      //the modal variable at scope of controller
      $scope.modal = modal;
  });

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });


}]);

function openModalHomeProductGraph($ionicModal, $scope, selected){
  $ionicModal.fromTemplateUrl(
    'js/directives/product-home/modal-home-product-graph.html', {

    }).then(function(modal) {
      modal.show();
      $scope.modal = modal;

      modal.scope.currentDate = $scope.currentDate
      //console.log('$scope', $scope)

      modal.scope.convertValue = convertValue;

      modal.scope.selected = selected
    },{
          scope: $scope,
          animation: 'scale-in'
    });
    $scope.closeModal = function() {
     $scope.modal.hide();
   };
   $scope.$on('modal.hidden', function() {
      $scope.modal.remove();
   });
}

function openModalProductHomeDist($ionicModal, $scope){
  $ionicModal.fromTemplateUrl(
    'js/directives/product-home/modal-product-home-dist.html', {

  }).then(function(modal) {
    modal.show();
    $scope.modal = modal;
  },{
        scope: $scope,
        animation: 'scale-in'
  });
  $scope.closeModal = function() {
   $scope.modal.hide();
 };
 $scope.$on('modal.hidden', function() {
    $scope.modal.remove();
 });
}

function openModalProductHome($ionicModal, $scope){
  $ionicModal.fromTemplateUrl('js/directives/product-home/modal-product-home.html', {

  }).then(function(modal) {
    modal.show();
    $scope.modal = modal;
  },{
        scope: $scope,
        animation: 'scale-in'
  });
  $scope.closeModal = function() {
   $scope.modal.hide();
 };
 $scope.$on('modal.hidden', function() {
    $scope.modal.remove();
 });
}


function openModalInternetFim($ionicModal, $scope, $state){
  $ionicModal.fromTemplateUrl('js/directives/product-home/modal-internet-fim.html', {
    scope: $scope,
    animation: 'scale-in'
  }).then(function(modal) {
      modal.show();
      $scope.modal = modal;
  });

  $scope.$parent.goCompraPacote();
  $scope.$parent.goDistribuir();
  $scope.$parent.closeModal();

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  $scope.$on('modal.hidden', function() {
    $scope.modal.remove();
  });
}


function openModalInternetQuaseFim($ionicModal, $scope, $state){
  $ionicModal.fromTemplateUrl('js/directives/product-home/modal-internet-quase-fim.html', {
    scope: $scope,
    animation: 'scale-in'
  }).then(function(modal) {
      modal.show();
      $scope.modal = modal;
  });

  $scope.$parent.goCompraPacote();
  $scope.$parent.goDistribuir();
  $scope.$parent.closeModal();

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  $scope.$on('modal.hidden', function() {
    $scope.modal.remove();
  });
}

function openModalInternetStart($ionicModal, $scope, $state){
  $ionicModal.fromTemplateUrl('js/directives/product-home/modal-home-product-start.html', {
    scope: $scope,
    animation: 'scale-in'
  }).then(function(modal) {
      modal.show();
      $scope.modal = modal;
  });

  $scope.$parent.goCompraPacote();
  $scope.$parent.goDistribuir();
  $scope.$parent.closeModal();

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  $scope.$on('modal.hidden', function() {
    $scope.modal.remove();
  });
}


function convertValue(value){
  //GB
  if(value > 999){
    value = (value/1000).toFixed(1) + " GB"
  } else {
    value = value + " MB"
  }

  return value;
}
