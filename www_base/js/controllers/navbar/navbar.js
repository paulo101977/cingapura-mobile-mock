angular.module('starter.controllers')
.controller('NavbarCtrl',
  [
    '$rootScope' ,
    '$scope' ,
    '$ionicSideMenuDelegate',
    '$state',
    '$ionicPopover',
    '$ionicHistory',
  function(
    $rootScope ,
    $scope,
    $ionicSideMenuDelegate ,
    $state,
    $ionicPopover,
    $ionicHistory) {

    $scope.transparenceClass = "";

    //hamburger
    $scope.showMenu = false;

    //navbar hide effect
    $rootScope.$watch('modalOverlay', function(newValue, oldValue){
      $scope.overlay = newValue;
    }, true)

    $rootScope.$watch('hasPopover', function(newValue, oldValue){
      $scope.hasPopover = newValue;
    }, true)


    //listen to route change
    $rootScope.$on('$stateChangeStart',
      function(event, toState, toParams, fromState, fromParams) {

        console.log($ionicHistory)

        //enable NavDrawer
        //$ionicSideMenuDelegate.canDragContent(false);
        if($scope.navPopover) {$scope.navPopover.hide()};

        if(toState.name == 'products-home'){
          //setup the popover
          $ionicPopover
          .fromTemplateUrl(
            'templates/navbar/popover-product-home.html', {
            scope: $scope,
          }).then(function(popover) {
            $scope.navPopover = popover;
          });
        }

        if(toState.name == 'distribchange'){
          //setup the popover
          $ionicPopover.fromTemplateUrl('templates/navbar/popover.html', {
            scope: $scope,
          }).then(function(popover) {
            $scope.popover = popover;
          });
        }

        if(toState.name == 'products.tab' || toState.name == 'buy-package'){
          //create watcher
          $scope.navdrawerListener =
            getNavDrawerListener($scope , $ionicSideMenuDelegate)

        } else {
          //close on backbutton
          $ionicSideMenuDelegate.toggleLeft(false);

          //destroy the listener
          if(!$ionicSideMenuDelegate.isOpen()){
            if($scope.navdrawerListener) $scope.navdrawerListener();
            $scope.navdrawerListener = null;
          }
        }

        if(toState.name == 'login' || toState.name == 'initial'){
          $scope.showMenu = false;
          hideOrShowNavbar($scope, false)
          $scope.transparenceClass = "";
        }
        else if(toState.name == 'distribsucess'){
          $scope.showMenu = false;
          hideOrShowNavbar($scope, false)
          $scope.transparenceClass = "";
          $ionicSideMenuDelegate.canDragContent(false);
        }
        else if(toState.name == 'transfer-balance-success'){
          $scope.showMenu = false;
          hideOrShowNavbar($scope, false)
          $scope.transparenceClass = "";
          $ionicSideMenuDelegate.canDragContent(false);
        }
        else if(toState.name == 'buy-package-success'){
          $scope.showMenu = false;
          hideOrShowNavbar($scope, false)
          $scope.transparenceClass = "";
          $ionicSideMenuDelegate.canDragContent(false);
        }
        else if(toState.name == 'products.tab' || toState.name == 'products'){
          $scope.showMenu = true;
          hideOrShowNavbar($scope, true)
          $scope.transparenceClass = "transparent";
          $ionicSideMenuDelegate.canDragContent(true);
        }
        else {
          $scope.showMenu = false;
          hideOrShowNavbar($scope, true)
          $scope.transparenceClass = "";
          $ionicSideMenuDelegate.canDragContent(false);
        }
      }
    );

    //show menu
    $scope.showPopover = function($event){
      /*if($scope.popover && ($state.current.name === 'distribchange')) {
        $scope.popover.show($event)
      } else if($state.current.name === 'products-home'){
        $scope.navPopover.show($event)
      }*/
      $scope.navPopover.show($event)
    }

    $scope.distribution = function(){
      var data = {};
      var max = 0;

      if($scope.hasPopover){
        angular.copy($scope.$parent.data, data);

        max = data.max;

        $scope.$parent.data.values.forEach(function(item, index){
          data.values[index].value = parseFloat(max/data.values.length).toFixed(2);
        })

        $rootScope.$broadcast('distributionData', data);
        if($scope.popover) $scope.popover.hide();
      }
    }

    $scope.reset = function(){
      if($scope.hasPopover){
        //broadcast original data
        $rootScope.$broadcast('resetData', $scope.$parent.data);
        if($scope.popover) $scope.popover.hide();
      }
    }

    $scope.openNavDrawer = function(){
      $ionicSideMenuDelegate.toggleLeft(true);
    }


    //clear popover instance
    $scope.$on('$destroy', function() {

      console.log('on destroy')
      if($scope.popover) $scope.popover.remove();

      /*
      if($scope.popover) $scope.popover.remove();

      //destroy the listener
      if($scope.navdrawerListener){
        $scope.navdrawerListener();
      }
      */
    });
  }
])

function getNavDrawerListener($scope , $ionicSideMenuDelegate){
  //NavDrawer listener
  return $scope.$watch(
    function () {
      return $ionicSideMenuDelegate.getOpenRatio();
    },
    function (ratio) {
      var width = +$('.navmenu-content').width();
      $('.navmenu-content').css({left: (+ratio)*width})
    }
  );
}

function hideOrShowNavbar($scope, arg){
    $scope.$parent.showNavbar = arg;
}
