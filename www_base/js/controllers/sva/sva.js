var dataSva = [{
    data: '(21) 99639-9999',
    have: true
  },{
    data: '(21) 99639-7565',
    have: false
  },{
    data: '(21) 99639-7562',
    have: true
  }]

var descr = `Lorem Ipsum é
              simplesmente uma simulação de
              texto da indústria tipográfica.
            `

var infoDataSVA = [
  {
    title: 'Te Ligou Pro',
    description: 'Saiba quem te ligou mesmo sem ter o contato salvo, bloqueie chamadas indesejadas e envie SMS para quem te ligou quando estiver fora de área.',
    url: 'http://wap.oi.com.br/plano/product.html?prod=12'
  },
  {
    title: 'Oi Áudio Livros',
    description: 'Serviço de assinatura de áudio livros com mais de 100 mil títulos pra ouvir no app ou no seu navegador.',
    url: 'http://wap.oi.com.br/plano/product.html?prod=13'
  },
  {
    title: 'Oi Revistas',
    description: 'Acesse uma banca digital com centenas de títulos nacionais na integra direto do seu celular, tablet ou computador.',
    url : 'http://wap.oi.com.br/plano/product.html?prod=4'
  },
  {
    title: 'Playkids & BTFIT',
    description: 'Playkids: uma biblioteca digital com milhares de histórias pra crianças  de até 8 anos. BTFIT: treine a qualquer hora e em qualquer lugar com um personal trainer online.',
    url: 'http://wap.oi.com.br/plano/pos_fun_plus'
  },
  {
    title: 'Para aprender',
    description: 'Com o serviço de Educação da Oi, você pode aprender com cursos profissionalizantes, empreendedorismo, línguas, preparatórios e muito mais.',
    url: 'http://wap.oi.com.br/plano/product.html?prod=16'
  }
]
angular.module('starter.controllers')
    .controller('SvaCtrl', [ '$scope' ,'$state', function($scope, $state) {

        $scope.selectData = dataSva;
        $scope.selected = "selecione";
        $scope.active = false;
        $scope.have = -1;
        $scope.infoDataSVA = infoDataSVA;

        $scope.open = function(){
          $scope.active = !$scope.active;
        }

        $scope.openUrl = function($event, url){
          $event.preventDefault();
          window.open(url, '_system', 'location=yes');
        }

        $scope.setSelected = function(selected){
          $scope.selected = selected.data;
          $scope.active = false;
          $scope.have = selected.have;
        }

        $scope.getClass = function(index){
          index = +index;

          switch (true) {
            case index === dataSva.length - 1:
              return 'last';
              break;
            case index === 0:
              return 'first';
              break;
            default:
              return '';
          }
        }

        //enable back button
        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });

        // function to submit the form after all validation has occurred
        $scope.backSateIni = function (){
            $state.go('products-home')
        }

    }

])
