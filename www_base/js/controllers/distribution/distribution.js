

const textHasDistribution = {
  title: "Ainda há franquia de internet pra distribuir.",
  content: 'Distribua por completo antes de clicar em “Aplicar”.'
}

const textTipDistribution = {
  title: "A franquia de internet já está distribuída por completo.",
  content: "Se quiser aumentar a internet de alguém,"
          + "é necessário diminuir a de outra pessoa."
}

let errorTxt = `Você precisa diminuir a internet de alguém
pra aumentar a de outra pessoa.`


function setText($scope, textObj){
  $scope.modalText = textObj;
}


angular.module('starter.controllers')
.controller('DistributionCtrl', [
  '$rootScope' , '$scope' , '$ionicHistory' , '$state',
  '$timeout', '$interval', 'dateService',
  function($rootScope, $scope, $ionicHistory,
    $state, $timeout , $interval, dateService) {
    //get from main controller
    //$scope.data = $scope.$parent.data;

    //deep copy original data
    $scope.data = {};
    $scope.original = $scope.$parent.data;
    $scope.warningMessage = errorTxt;

    //copy to data variable
    angular.copy($scope.$parent.data, $scope.data);

    //copy main scope hasDistribution to local variable
    $scope.hasDistribution = $scope.$parent.hasDistribution;


    //for the animation
    $scope.idInterval = -1;

    //has animation
    $scope.hasAnimation = true;

    //disable input over the animation
    $scope.inputDisabled = true;

    //call parseInt
    $scope.parseInt = parseInt;

    //there are not current to distribute
    $scope.current = 0;
    $scope.currentStep = 0.1;

    //is green and check
    $scope.allOk = "";

    //show warning after try change input on current equal zero
    $scope.showWarningMessage = false;
    $scope.isFirst = true;
    $scope.idWarningTimeout = -1;
    $scope.iteration = 0;

    //'28/04/2018' //example
    $scope.currentDate = dateService.getCurrentDate();

    $scope.goDistributionChange = function(){
      $state.go('distribchange')
    }

    $scope.formatFloatNumber = function(number){
      number = +number;
      return parseFloat(number).toFixed(2);
    }

    //if current route is distribchange then set the attrs
    //and add listener
    if($state.current.name === 'distribchange'){
      //open modal
      setText($scope, textTipDistribution);
      animate($scope,$timeout,$rootScope,$interval);
    }
    else if($state.current.name === 'distribinfo'
      || $state.current.name == 'distribsucess'){

      angular.copy($scope.$parent.dataAfterChange , $scope.data);
    }

    $scope.showInfo = function(){
      $state.go('distribinfo')
    }

    $scope.openModal = function(){
      $scope.modalClass = 'active';
      $rootScope.modalOverlay = 'active';
    }

    $scope.closeModal = function(){
      $scope.modalClass = "";
      $rootScope.modalOverlay = '';
    }

    $scope.apply = function(){
      if($scope.current > 0){
        //scroll top
        $('.c-distribution').stop().animate({scrollTop:0}, 500, 'swing');

        //set modal with other text
        setModalInstanceDistribution($scope, $timeout)
        setText($scope, textHasDistribution);
        openModal($scope,$rootScope);
      } else {
        //apply change
        //angular.copy($scope.data ,$scope.$parent.data);
        angular.copy($scope.data ,$scope.$parent.dataAfterChange);

        $scope.$parent.hasDistribution = true;

        $state.go('distribsucess')
      }
    }

    $scope.goToInit = function(){
      //disable back button
      /*$ionicHistory.nextViewOptions({
        disableBack: true
      });*/

      $state.go('products-home')
    }

    $scope.$on("$destroy", function() {
        $rootScope.modalOverlay = '';
        //clearInterval($scope.idInterval);
        clearTimeout($scope.idWarningTimeout);
        if($scope.idInterval) $interval.cancel($scope.idInterval);
    });

    $scope.$on('distributionData', function(event, newDataValue){
        event.preventDefault();

        applyChanges($scope, $timeout , newDataValue);
    })

    //listen to resetData and copy original to data
    $scope.$on('resetData', function(event, oldDataValue) {

      event.preventDefault();


      applyChanges($scope, $timeout , oldDataValue);
    });
}])

function applyChanges($scope, $timeout , newValues){
  var width = +$('#c-input-0').width();
  var max = +newValues.max;


  //remove the watcher
  $scope.watcherData();


  //angular.copy(newValues,$scope.data);
  angular.copy(newValues,$scope.data);
  $scope.current = 0;

  $timeout(function(){
    newValues.values.forEach(function(item, index){
      var currentValue = +item.value;
      var currentWidth = +currentValue/max*width;
      var selector = '.bg-before-' + (+index + 1);
      var el = $(selector);
      el.css({width:currentWidth + 'px'})

    })

    watcherData($scope);
  })
}

function animate($scope,$timeout,$rootScope,$interval){

  //disable menu functions
  $rootScope.hasPopover = false;

  $timeout(function(){
    $scope.current = $scope.data.max;

    var size = $scope.data.values.length;

    //set initial
    $scope.data.values.forEach(function(item, index){
      item.value = 0;
      $('.bg-before-' + (+index + 1)).width('0px');
    })

    //index == 0
    setTimeout(function(){
      animateSliders($scope, size , 0 , $rootScope,$timeout,$interval);
    }, 400)
  })
}

function animateSliders($scope, size , index,$rootScope,$timeout, $interval){
    var original = +$scope.original.values[index].value;
    var currentValue = 0;

    var max = +$scope.data.max;
    var width = +$('#c-input-0').width();
    var idInterval = -1;

    idInterval = $interval(function(){

      var sum = 0, value = 0, userValue = 0;

      if(currentValue >= original){
        index++;
      }

      //animation end
      if(index > size - 1){
        $interval.cancel($scope.idInterval);
        $scope.currentStep = 0.1;

        setModalInstanceDistribution($scope, $timeout)
        openModalApply($scope,$rootScope);
        setInputsListerers($timeout, $scope);

        //enable functions on menu
        $rootScope.hasPopover = true;
      }

      //change data
      if(index > $scope.original.values.length - 1) return;
      original = +$scope.original.values[index].value;
      currentValue = +$scope.data.values[index].value;
      currentValue += $scope.currentStep;

      //change bar bg
      $('.bg-before-' + (+index + 1)).width(currentValue/max*width + 'px');
      //$scope.$apply(function(){
      $scope.data.values[index].value = currentValue;
      sum = getSum($scope.data.values)
      var value = parseFloat(max - sum).toFixed(2);
      if(value >= 0){
        $scope.current = value;
      } else {
        $scope.current = parseFloat('0.00').toFixed(2);
      }

      //TODO: remove if it is necessary
      if($scope.data.values[index].value > original){
        $scope.data.values[index].value = original;
      }

      //})
    }, 2);

    $scope.$apply(function(){
      $scope.idInterval = idInterval;
    });
}

function setInputsListerers($timeout, $scope){
  $timeout(function(){
    //watch to change in data.values
    watcherData($scope);
  })
}

function watcherData($scope){
  $scope.watcherData = $scope.$watch('data.values', function(newValue, oldValue){

    var max = +$scope.data.max;
    var width = +$('#c-input-0').width();

    newValue.forEach(function(item, index){
      var value = +item.value;
      $('.bg-before-' + (+index + 1)).width(value/max*width + 'px');

      if(value != oldValue[index].value){
        var sum = getSum(newValue);

        if(sum > max){
          value = value - (sum - max);
          value = parseFloat(value).toFixed(2);
          //value = value - diff;
          $scope.data.values[index].value = value;
        } else {
          var current = +(max - sum);
          $scope.current = current.toFixed(2);


          if($scope.current == 0) {
            $scope.allOk = "active";
            //$scope.iteration = 0;

            if($scope.isFirst){
              $scope.showWarningMessage = true;
              $scope.isFirst = false;
              $scope.warningMessage = errorTxt;
              timeoutShowMessage($scope)
            }

            if(!$scope.isFirst){
              clearTimeout($scope.idWarningTimeout);
              $scope.idWarningTimeout= setTimeout(function(){
                $scope.iteration++;
              },700)
            }

            if($scope.iteration > 0){
              $scope.showWarningMessage = true;
              $scope.warningMessage = errorTxt;
              timeoutShowMessage($scope);
            }

          }
          else {
            $scope.allOk = "";
            $scope.showWarningMessage = false;
            $scope.isFirst = false;
            $scope.iteration = 0;
            $scope.warningMessage = "";
          }

          /*if(value <= 0.1){
            $scope.data.values[index].value = 0.1;
            $scope.showWarningMessage = true;
            $scope.warningMessage = valueTxt;
            timeoutShowMessage($scope);
            console.log('showWarningMessage')
          }*/

        }
      }
    })
  }, true);
}

function timeoutShowMessage($scope){
  setTimeout(function(){
    $scope.$apply(function(){
      $scope.showWarningMessage = false;
    })
  },3000)
}

function getSum(values){
  var sum = 0;
  values.forEach(function(item, index){
    sum += +item.value;
  })

  return sum;
}

function setModalInstanceDistribution($scope, $timeout, modalText){
  $timeout(function(){
    var  height = +$('.c-main-content-wrapper-distribution').height();
    var el = $('.c-panel-info .current');
    var x = +el.position().left + el.width()/2;

    //height + navbar
    $('.c-modal-distribution').height(height + 57);

    $('.c-modal-arrow').css({'left': x });
  })

}

function openModal($scope,$rootScope){
  $scope.modalClass = 'active';
  $rootScope.modalOverlay = 'active';
}

function openModalApply($scope, $rootScope){
  //$rootScope.$apply(function(){
    $scope.modalClass = 'active';
    $rootScope.modalOverlay = 'active';
    $scope.hasAnimation = false;
    $scope.inputDisabled = false;//enable inputs
    angular.copy($scope.$parent.data, $scope.data);
  //})
}
