angular.module('starter.controllers')
.directive('selectMenu', function($timeout) {
  return {
      restrict : "E",
      templateUrl : "directives/select-menu/select-menu.html",
      controller:'@', //specify the controller is an attribute
      name:'ctrlName' //name of the attribute.
  };
})
