angular.module('starter.controllers')
.directive('distributionDisplay', function($timeout, $compile) {
  return {
      restrict : "E",
      replace: true,
      controller: __controller,
      link: function(scope, element, attrs, ctrl, transclude){

      },
      templateUrl : "directives/distribution/distribution-display.html",
      //controller: DistributionDisplayCtrl
  };
})

function __controller($scope, $timeout){
  //console.log('$scope', $scope)

  $timeout(function(){
    //var width = $('.c-bar-container').width();
    var width = $('body').width() - 32;// minus padding
    var max = +$scope.$parent.data.max;

    //each
    $scope.$parent.data.values.forEach(function(elValue, index){
      var value = +elValue.value;

      var el = $('div[datachange] #c-bar-' + index);

      el = el.length > 0 ? el : $('#c-bar-' + index);
      el
        .css('width' , value/max*width + 'px');


    })//end each

  })
}
