
//const
const w = h = 205;
const innerRadius = 95;  //const innerRadius = 85;

let colors = [
  {
    cor1: '#fe6b03',
    cor2: '#e33465',
  },
  {
    cor1: '#00d213',
    cor2: '#00baf7',
  },{
    cor1: '#ffd700',
    cor2: '#f8562c',
  }
]

angular.module('starter.controllers')
.directive('donutChart', function($timeout) {
  return {
      restrict : "E",
      templateUrl : "directives/donutchart/donutchart.html",
      link: function(scope, element, attrs){

        //set the id of the container
        scope.idElement = attrs.idelement;
        //wait this render
        $timeout(function(){
            var  data = angular.fromJson(attrs.data);
            var dataset = [];
            var index = +attrs.idelement.split('chart')[1];
            var corgraph1 = ""
            var corgraph2 = ""

            var color = [];
            data.value = +data.value

            if(data.value == 0){
              color = colors[0]
            } else if(data.value < 0.5){
              color = colors[2]
            } else {
              color = colors[1]
            }


            corgraph1 = color.cor1
            corgraph2 = color.cor2

            dataset.push({valor:+data.value}, {valor:+data.value})


            drawChart(
              attrs.idelement,
              dataset,
              null ,
              index,
              corgraph1,
              corgraph2
            )
        })
      }
  };
})

function drawChart(chart , dataset, avulso , index, corgraph1, corgraph2){

    //var element = document.getElementsByClassName("swiper-wrapper");
    //element.classList.add("transform-swiper-wrapper");

    var pie=d3.layout.pie()
      .value(function(d){return d.valor})
      .sort(null)
      .padAngle(.03);

    //var w=200,h=200;

    var outerRadius=w/2;
    //var innerRadius=80;

    //var color = d3.scale.category10();
    var green = "#19c40b";
    var gray = "#f5f5f5";
    var red = "#f8562c";

    var maxAngle = 360;  //var maxAngle = 270;
    var radiusMax = maxAngle * (Math.PI/180);

    var total = (avulso) ? (dataset[1].valor + avulso) : dataset[1].valor;

    //padding to linear-gradient start angle
    //var angleToPadding = (dataset[0].valor)/total*360;
    var angleToPadding = 360;

    //var range = d3.scale.linear().domain([0, maxAngle]).range([45, 15]);
    var range = d3.scale.linear().domain([0, maxAngle]).range([45, 15]);
    //if this is more than 90 deg
    //console.log('angleToPadding' , range(angleToPadding))
    //angleToPadding = (angleToPadding > maxAngle/2) ? maxAngle : angleToPadding;

    //var tweenValue = ((dataset[0].valor)/total*radiusMax);
    var tweenValue = (radiusMax);

    var arc=d3.svg.arc()
      .outerRadius(outerRadius)
      .innerRadius(innerRadius)
      .cornerRadius(50)
      .startAngle(0)

    var svg=d3.select("#chart" + index)
      .append("svg")
      .attr({
          id: 'svg' + index,
          width:w,
          height:h,
          class:'shadow'
      })
      .append('g')
      .attr({
          transform:'translate('+w/2+','+h/2+')'
      });

    var defs = svg.append("svg:defs")

    var red_gradient = defs.append("svg:linearGradient")
      .attr("id", "gradient" + index)
      .attr("x1", "0%")
      .attr("y1", "-10%")
      .attr("x2", "0%")
      .attr("y2", "100%")
      .attr('gradientTransform','rotate(' + range(angleToPadding) + ')')
      .attr("spreadMethod", "pad");

  red_gradient.append("svg:stop")
      .attr("offset", "0%")
      .attr("stop-color", corgraph1) //#00d213
      .attr("stop-opacity", 1);

  red_gradient.append("svg:stop")
      .attr("offset", "100%")
      .attr("stop-color", corgraph2) //#dc2382
      .attr("stop-opacity", 1);


      var background =svg
          .append('path')
          .datum(radiusMax)
          .attr({
              'class': 'background',
              'fill':gray
          })
          .attr('d', function(d) { return arc.endAngle(d)(); })

      var foreground=svg
        .append('path')
        .datum(0)
        .attr({
            'class': 'foreground',
            'fill':"url(#gradient" + index + ")"
        })
        .attr('d', function(d) { return arc.endAngle(d)(); })
        .each(function(d) { this._current = d; });


      setTimeout(function(){
        animateArc(index, dataset, avulso);
      }, 500)

   }

   function animateArc(index, dataset, avulso){


     var maxAngle = 360;  //var maxAngle = 270;
     var radiusMax = maxAngle * (Math.PI/180);
     var total = (avulso) ? (dataset[1].valor + avulso) : dataset[1].valor;
     //var tweenValue = ((dataset[0].valor)/total*radiusMax);
     var tweenValue = (radiusMax);
     //var w=200,h=200;
     var outerRadius=w/2;
     //var innerRadius=80;

     var arc=d3.svg.arc()
       .outerRadius(outerRadius)
       .innerRadius(innerRadius)
       .cornerRadius(50)
       .startAngle(0)

       //console.log('#svg' , '#svg' + index)


    d3.select('#svg' + index)
         .select('path.foreground')
         .datum(0)//reset
         .attr('d', function(d) { return arc.endAngle(0)(); })

     d3.select('#svg' + index)
       .select('path.foreground')
       .datum(tweenValue)
       .transition()
       .delay(0)      //.delay(500)
       .duration(0)  //.duration(1000)
       .attrTween("d", function(d){
         var interpolate = d3.interpolate(this._current, d);
         this._current = interpolate(0);

         return function(t){
             return arc.endAngle(interpolate(t))();
         }
       });
   }
