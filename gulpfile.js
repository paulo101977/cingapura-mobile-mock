var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCss = require('gulp-clean-css');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var shell = require('gulp-shell');
var autoprefixer = require('gulp-autoprefixer');
var templateCache = require('gulp-angular-templatecache');
var ngAnnotate = require('gulp-ng-annotate');
var useref = require('gulp-useref');
var clean = require('gulp-clean');

//path to watch
var paths = {
  sass: './scss/**/*.scss',
  templateCache: './www_base/templates/**/*.html',
  ng_annotate: './www_base/js/*.js',
  useref: './www_base/*.html'
};

gulp.task('clean', function(done){
  gulp.src('www', {read: false})
    .pipe(clean())
    .on('end', done)
})



gulp.task('templatecache', function (done) {
  gulp.src(['./www/templates/**/*.html', './www/js/**/*.html'])
    .pipe(templateCache({standalone:true}))
    .pipe(gulp.dest('./www/templatejs/'))
    .on('end', done);
});

gulp.task('ng_annotate', function (done) {
  gulp.src('./www/js/**/*.js')
  .pipe(ngAnnotate({single_quotes: true}))
  .pipe(gulp.dest('./www/dist/dist_js/app'))
  .on('end', done);
});

gulp.task('useref', function(done){
  gulp.src('./www_base/*.html')
      .pipe(useref())
      .pipe(gulp.dest('./www/'))
      .on('end', done);
})

var paths = {
  sass: ['./scss/**/*.scss']
};

//add build minify
gulp.task('run' ,shell.task(
  [
        'gulp clean;'
        + 'mkdir ./www;'
        + 'cp -rp ./www_base/ ./www;'
        + 'gulp build;'
        + 'gulp watch & ionic serve --lab;'
  ])
)



gulp.task('default', ['watch']);

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        })
    )
    .pipe(gulp.dest('./www/css/'))
    .pipe(cleanCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});



gulp.task('build' ,shell.task(
  [
    'gulp clean;'
    + 'mkdir ./www;'
    + 'cp -rp ./www_base/ ./www;'
    + 'gulp exec:build;'
  ])
)

gulp.task('exec:build', shell.task([
  'gulp sass;'
  + 'gulp templatecache;'
  + 'gulp ng_annotate;'
  + 'gulp useref;'
]));

gulp.task('exec:build:update', shell.task([
  'gulp clean;'
  + 'mkdir ./www;'
  + 'cp -rp ./www_base/ ./www;'
  + 'gulp sass;'
  + 'gulp templatecache;'
  + 'gulp ng_annotate;'
  + 'gulp useref;'
]));


gulp.task('watch', function() {
  //gulp.watch(paths.sass, ['build']);
  gulp.watch([
    './scss/**/*.scss',
    './www_base/templates/**/*.html',
    './www_base/js/**/*.*',
    './www_base/*.html'
  ], ['exec:build:update']);
});
